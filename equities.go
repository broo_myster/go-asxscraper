// Pull equities from ASX API using downloaded company list

package asxscraper

import (
  "io/ioutil"
  "encoding/json"
  _ "github.com/go-sql-driver/mysql" //MySQL driver
	"database/sql"
  "fmt"
  "net/http"
  "time"
  "math/rand"
)

type asxQuote struct {
  equity string
  openPrice float64
  highPrice float64
  lowPrice float64
  closePrice float64
  volume int
}

// PullEquities is the main loop for retrieving the full list of equities
func PullEquities() (error) {
  var result asxQuote

  // Connect to MySQL
	db, err := sql.Open("mysql", DbConnect())
	if err != nil {
		return err
	}
  defer db.Close()

  // Build a date string for today
  t := time.Now()
  dateString := t.Format("2006-01-02")

  //  Prepare the MySQL insert statement
  statementInsert, err := db.Prepare("INSERT INTO prices VALUES(?,?,?,?,?,?,?)")
  if err != nil {
      return err
  }
  defer statementInsert.Close()

  // Extract a list of equities to be queried from the companies table
  rows, err := db.Query("SELECT ticker FROM companies")
  if err != nil {
      return err
  }
  defer rows.Close()

  Slack("Downloading daily closing prices")

  // Cycle through all equities and query the ASX API to get a quote
  for rows.Next() {
    var ticker string

    // Get the ticker code
    err = rows.Scan(&ticker)
    if err != nil {
        return err
    }

    // Query the ASX API to get a quote
    result, err = pullPrice(ticker)
    if err != nil {
      Log.Warning(err)
    } else {
      // If the response is valid, then insert the quote into MySQL
      _, err = statementInsert.Exec(result.equity,
                                  dateString,
                                  result.openPrice,
                                  result.highPrice,
                                  result.lowPrice,
                                  result.closePrice,
                                  result.volume)
      if err != nil {
        return err
      }

    }

    // Be a good internet citizen and don't slam the API.
    // They're only closing prices so there's no need to rush
    time.Sleep(
      (time.Duration(configuration.pollInterval) * time.Millisecond) +
      (time.Duration(rand.Intn(3000)) * time.Millisecond))
  }

  if err != nil {
    Slack("Closing price collection exited with errors (check the log)")
  } else {
    Slack("Closing price collection completed ok")
  }

  return err
}

// PullPrice retrieves the current (delayed) price statistics from the ASX API
func pullPrice(share string) (asxQuote, error) {
  var c interface{} //The interface to be used for the response
  var q asxQuote //Structure for equity quote

  fullURL := fmt.Sprintf("%s%s", configuration.asxAPI, share)

  // Create and object for the API call and query the URL
  apiClient := http.Client{Timeout: 10 * time.Second}
  r, err := apiClient.Get(fullURL)
  if err != nil {
      return q, err
  }
  defer r.Body.Close()

  // Extract the JSON from the body of the response
  body, err := ioutil.ReadAll(r.Body)
  if err != nil {
      return q, err
  }

  // ...and process the JSON into a map
  err = json.Unmarshal(body, &c)
  if err != nil {
     return q, err
  }

  // Parse and process the JSON configuration if it contains a valid quote
	m := c.(map[string]interface{})
  Log.Debug(m)
  // Some responses don't contain quote data, so filter these out
  if _, ok := m["open_price"]; ok {
    q.equity = m["code"].(string)
    q.openPrice = m["open_price"].(float64)
    q.highPrice = m["day_high_price"].(float64)
    q.lowPrice = m["day_low_price"].(float64)
    q.closePrice = m["last_price"].(float64)
    q.volume = int(m["volume"].(float64))
  } else {
    err = fmt.Errorf("%s: Missing equity stats", share)
  }
  return q, err
}

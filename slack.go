package asxscraper

import (
        "github.com/ashwanthkumar/slack-go-webhook"
)

// Slack sends a string to a preconfigured webhook
func Slack(s string) {
    webhookUrl := configuration.slackURL

    payload := slack.Payload {
      Text: s,
      Username: "Share Analyser",
      Channel: "#housebot",
      IconEmoji: ":moneybag:",
    }
    _ = slack.Send(webhookUrl, "", payload)

    return 
}




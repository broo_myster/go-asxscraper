# ASX Scraper

Golang application to pull closing prices from ASX.  Collects latest ASX equity list and then uses that to pull prices at a predeficed rate.

##Installation

asx-scraper requires MySQL with access and credential details stored in the config file.  Table schema as follows:

```
mysql> show tables;
+---------------+
| Tables_in_asx |
+---------------+
| companies     |
| holidays      |
| prices        |
+---------------+
3 rows in set (0.00 sec)

mysql> describe prices;
+--------+---------------+------+-----+---------+-------+
| Field  | Type          | Null | Key | Default | Extra |
+--------+---------------+------+-----+---------+-------+
| ticker | varchar(10)   | YES  | MUL | NULL    |       |
| date   | date          | YES  | MUL | NULL    |       |
| open   | decimal(15,4) | YES  |     | NULL    |       |
| high   | decimal(15,4) | YES  |     | NULL    |       |
| low    | decimal(15,4) | YES  |     | NULL    |       |
| close  | decimal(15,4) | YES  |     | NULL    |       |
| volume | int(11)       | YES  |     | NULL    |       |
+--------+---------------+------+-----+---------+-------+
7 rows in set (0.00 sec)

mysql> describe holidays;
+-------+------+------+-----+---------+-------+
| Field | Type | Null | Key | Default | Extra |
+-------+------+------+-----+---------+-------+
| date  | date | YES  |     | NULL    |       |
+-------+------+------+-----+---------+-------+
1 row in set (0.00 sec)

mysql> describe companies;
+--------+--------------+------+-----+---------+-------+
| Field  | Type         | Null | Key | Default | Extra |
+--------+--------------+------+-----+---------+-------+
| ticker | varchar(10)  | NO   | PRI | NULL    |       |
| name   | varchar(100) | YES  |     | NULL    |       |
| gics   | varchar(100) | YES  |     | NULL    |       |
+--------+--------------+------+-----+---------+-------+
3 rows in set (0.00 sec)

+------------------------------------------------------------------------------------+
| Grants for asx-scraper@localhost                                                   |
+------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO 'asx-scraper'@'localhost'                                    |
| GRANT SELECT, INSERT, CREATE, UPDATE, DROP ON `asx`.* TO 'asx-scraper'@'localhost' |
+------------------------------------------------------------------------------------+

```

##Usage

```asx-scraper [-v] [-c <full path to config file>]```

asx-scraper can be set up as a daily cron task.  Before executing a pull from ASX, it will check that it's a trading day.  Saturdays and Sundays are automaticall excluded.  Any dates listed inteh holidays table are also excluded.  Trading holidays can be found on the ASX website.

```21 17 * * * /home/wayne/go/src/bitbucket.org/broo_myster/go-asxscraper/cmd/asx-scraper >> /var/log/asx-scraper.log 2>&1

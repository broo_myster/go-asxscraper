package main

import (
	"os"

	asxscraper "bitbucket.org/broo_myster/go-asxscraper"
	"github.com/galdor/go-cmdline"
	"github.com/op/go-logging"
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
//	"time"
//	"math/rand"
)

var logLevel = logging.INFO
var configFile = "/etc/asx-scraper/config.json"

func main() {
	// Parse command line arguments
	// config file path
	// log level
	cmdLine := cmdline.New()
	cmdLine.AddOption("c", "config", "file", "Full path and name of config file (default /etc/asx-scraper/config.json)")
	cmdLine.AddFlag("v", "verbose", "Verbose/Debug output")
	cmdLine.Parse(os.Args)

	if cmdLine.IsOptionSet("v") {
		logLevel = logging.DEBUG
	}

	if cmdLine.IsOptionSet("c") {
		configFile = cmdLine.OptionValue("c")
	}

	// Set up levelled logging
	asxscraper.LogLevel = logLevel
	asxscraper.Log = asxscraper.SetupLogger()

	// Load and validate config (nodes, scenarios)
	asxscraper.ConfigFile = configFile
	err := asxscraper.LoadConfig(configFile)
	if err != nil {
		os.Exit(1)
	}
	asxscraper.Log.Info("Configuration loaded OK")


  // Check that MySQL is alive and schema is present
	db, err := sql.Open("mysql", asxscraper.DbConnect())
	if err != nil {
		asxscraper.Log.Critical(err)
		os.Exit(1)
	}

	db.Close()
  asxscraper.Log.Debugf("Connected to %s", asxscraper.DbConnect())

  // Check to see that today is a trading day.
    // If it is, then get the latest company list and prices
    if asxscraper.TradingDay() {
      // Pull equity list and update MySQL
      errorCheck(asxscraper.PullCompanies())

      // Cycle through equities to extract Start main rule evaluation loop and event listener
      errorCheck(asxscraper.PullEquities())
    } else {
      asxscraper.Log.Info("Skipping weekend")
  }
}

// errorCheck is just a simple error checker and gradeful exit
func errorCheck(e error) {
    if e != nil {
      asxscraper.Log.Critical(e)
      os.Exit(1)
    }
}

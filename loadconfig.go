// processconfig parses the config file at startup.  Variables are loaded into
// Redis. Nodes and types are loaded into memory.  Rules are validated and
// loaded into memory.

package asxscraper

import (
	"encoding/json"
	"io/ioutil"
)

// Settings struct holds the config details for data collection and processing
//   asxAPI: URL for ASX equity price and details
//   pollInterval: time in milliseconds between polls
//   dbHost, dbUser, dbPass: host details and credentials for MySQL database
//   dbSchema, dbPrices, dbCompanies: schema and table names for ASX prices and companies
//   dbHolidays: trading holidays for 2018 and 2019
type Settings struct {
	asxAPI string
	asxList string
  pollInterval int
  dbHost string
	dbPort string
  dbUser string
  dbPass string
  dbSchema string
  dbPrices string
  dbCompanies string
  dbHolidays string
  slackURL string
}

// ConfigFile is the global file/path for application config
var ConfigFile = "/etc/asx-scraper/config.json"

// Variable structure for config
var configuration Settings

// MySql connect string
var dbConnect string

// LoadConfig reads the JSON config file into variables
func LoadConfig(configFile string) (err error) {

	var c interface{} //The interface to be used for the config

	// Open & read config file
	raw, err := ioutil.ReadFile(configFile)
	if err != nil {
		return
	}

	// Extract JSON from byte stream
	err = json.Unmarshal(raw, &c)
	if err != nil {
		Log.Errorf("Failed to parse config JSON: %s", err)
		return
	}

	// Parse and process the JSON configuration
	m := c.(map[string]interface{})
  configuration.asxAPI = m["asx-api"].(string)
	configuration.asxList = m["asx-list"].(string)
	configuration.pollInterval = int(m["interval"].(float64))
	configuration.dbHost = m["db-host"].(string)
	configuration.dbUser = m["db-user"].(string)
	configuration.dbPass = m["db-pass"].(string)
	configuration.dbSchema = m["db-schema"].(string)
	configuration.dbPrices = m["db-prices"].(string)
	configuration.dbCompanies = m["db-prices"].(string)
	configuration.dbHolidays = m["db-prices"].(string)
	configuration.slackURL = m["slackURL"].(string)

	return
}

// DbConnect forms the MySQL connection string using config
func DbConnect() (string) {
  return configuration.dbUser + ":" +
		            configuration.dbPass + "@tcp(" +
								configuration.dbHost + ":3306)/" +
								configuration.dbSchema + "?charset=utf8"

}

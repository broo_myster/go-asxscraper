// Pull latest csv file with company info and stor ti in MySQL

package asxscraper

import (
  "os"
  "io"
  "net/http"
  "time"
  "encoding/csv"
  "bufio"
  _ "github.com/go-sql-driver/mysql"  //MySQL driver
	"database/sql"
)


// PullCompanies imports the company file from URL in config file and process into MySQL
func PullCompanies() (err error) {
  // Download latest company file from company URL in config file
  Log.Info("Retrieving company file...")
  err = downloadCompanyFile("/tmp/asx-list.csv", configuration.asxList)
  if err != nil {
    Log.Critical("Failed to retrieve ASX company file")
    return
  }
  Log.Info("ASX company file retrieved OK.  Processing...")


  // Process the file and update the company table in MySQL
  Log.Info("Parsing ASX company file...")

  err = parseAndStore()
  if err != nil {
    Log.Critical("Failed to parse ASX company file")
    return
  }
  Log.Info("ASX company file parsed OK.")
  time.Sleep(5 * time.Second)

  return
}


// downloadFile will download a url to a local file.
func downloadCompanyFile(filepath string, url string) error {

    // Create the file
    out, err := os.Create(filepath)
    if err != nil {
        return err
    }
    defer out.Close()

    // Get the data
    resp, err := http.Get(url)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Write the body to file
    _, err = io.Copy(out, resp.Body)
    if err != nil {
        return err
    }

    return nil
}


// parseAndStore takes the new company file and import into MySQL
func parseAndStore() (err error) {
  // Open the csv file
  Log.Info("Opening company file...")

  f, err := os.Open("/tmp/asx-list.csv")
  if err != nil {
      return err
  }

  // Prepare MySQL table
  // drop table if exists companies; create table companies (ticker VARCHAR(10), name VARCHAR(100), gics VARCHAR(100));
  // Connect to MySQL
        Log.Info("Connecting to DB...")

	db, err := sql.Open("mysql", DbConnect())
	if err != nil {
		return err
	}
  defer db.Close()

  Log.Info("Preparing the insert statement...")

  //  Prepare the MySQL insert statement
  statementInsert, err := db.Prepare("INSERT INTO companies VALUES(?,?,?) ON DUPLICATE KEY update name=?, gics=?")
  if err != nil {
      return err
  }
  defer statementInsert.Close()

  Log.Info("Preparing the file buffer...")

  // Create a new csv reader
  r := csv.NewReader(bufio.NewReader(f))
  r.Comma = ','
  r.FieldsPerRecord = -1  // Variable number of fields so the header lines are processed

  Log.Info("Parsing the CSV file...")

  for {
    line, err := r.Read()
    if err == io.EOF {
      // End of file
      break
    }

    // Catch any parsing errors
    if err != nil {
      return err
    }

    if (len(line) == 3) && (line[0] != "Company name") {

      _, err = statementInsert.Exec(line[1],
                                  line[0],
                                  line[2],
                                  line[0],
                                  line[2])
      if err != nil {
        return err
      }
    }
  }

  return
}

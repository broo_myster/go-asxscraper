// Check today's date against the holiday table in MySQL

package asxscraper

import (
  "os"
  _ "github.com/go-sql-driver/mysql" //MySQL driver
	"database/sql"
  "time"
  "fmt"
)


// TradingDay looks for a matching date in the holidays table in MySQL
func TradingDay() (bool) {

  // Connect to MySQL
  db, err := sql.Open("mysql", DbConnect())
  if err != nil {
    Log.Critical(err)
    os.Exit(1)
  }
  defer db.Close()

  // Build a date string for today
  t := time.Now()

  // No trading on weekends
  DayOfWeek := int(t.Weekday())

  if (DayOfWeek == 0) || (DayOfWeek == 6) {
    return false
  }

  dateString := t.Format("2006-01-02")

  var count string

  // Search for dates matching today's date
  query := fmt.Sprintf("SELECT date FROM holidays WHERE date = '%s'", dateString)
  if err := db.QueryRow(query).Scan(&count); err == nil {
    // There's a matching row, so it's a holiday
    Log.Infof("%s is not a trading day. Exiting.", dateString)
    return false

  } else if err == sql.ErrNoRows {
    // No result, so it must be a trading day
    Log.Infof("%s is a trading day. Processing...", dateString)
    return true

  } else {
    // Panic!
    Log.Critical(err)
    os.Exit(1)

  }
  return false
}
